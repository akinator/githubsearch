package com.example.githubsearch

import android.app.Application
import com.example.githubsearch.networking.networkingModule
import com.example.githubsearch.repositorysearch.repositorySearchModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree


class GithubSearchApplication : Application() {


    override fun onCreate() {
        super.onCreate()

        Timber.plant(DebugTree())

        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger()

            // use the Android context given there
            androidContext(this@GithubSearchApplication)

            modules(repositorySearchModule, networkingModule)
        }
    }
}