package com.example.githubsearch.repositorysearch

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.githubsearch.dataobjects.Repository
import com.example.githubsearch.dataobjects.RepositorySearchResponse
import com.example.githubsearch.dataobjects.LOADING_REPOSITORY_ID
import com.example.githubsearch.networking.GithubApi
import kotlinx.coroutines.*
import org.koin.java.KoinJavaComponent.inject
import retrofit2.Response
import timber.log.Timber
import kotlin.math.ceil

private const val PER_PAGE: Int = 30
private const val MINIMUM_CHARACTERS: Int = 3

class GithubRepositorySearchViewModel : ViewModel() {

    private val githubApi : GithubApi by inject(GithubApi::class.java)

    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(
        viewModelJob + Dispatchers.Main
    )


    private var getPropertiesDeferred: Deferred<Response<RepositorySearchResponse>>? = null
    private var currentRequest = Job()

    private val loadingRepository: Repository = Repository(LOADING_REPOSITORY_ID)
    private var isLoadingMore: Boolean = false


    private var currentText: String = ""
    private var currentPage: Int = 1
    private var lastPage: Int = 0


    var searchKeyword: String? = null
        set(value) {
            Log.d("searchKeyword", value!!)

            if (field != value) {
                field = value
                onSearchEntered(value)
            }
        }

    private var _repositoriesTotalCount = MutableLiveData<Int>()

    val repositoriesTotalCount: LiveData<Int>
        get() = _repositoriesTotalCount


    private var _repositories = MutableLiveData<ArrayList<Repository>?>()

    val repositories: LiveData<ArrayList<Repository>?>
        get() = _repositories

    private var _isLoading = MutableLiveData<Boolean>()

    val isLoading: LiveData<Boolean>
        get() = _isLoading

    init {
        onExceptionSearch()

    }


    private fun searchGithubRepositories(filterText: String, page: Int) {

        sendRequest(
            filterText,
            page,
            { startSearchAnimation() },
            ::onSuccessSearch,
            { onExceptionSearch() },
            {})
    }

    private fun onExceptionSearch() {
        _repositoriesTotalCount.value = 0
        stopSearchAnimation()
    }

    private fun onSuccessSearch(response: Response<RepositorySearchResponse>) {

        val searchResponse: RepositorySearchResponse? = response.body()

        currentPage = 1
        lastPage = ceil((searchResponse!!.totalCount).toDouble() / PER_PAGE.toDouble()).toInt()
        _repositoriesTotalCount.value = searchResponse.totalCount

        _repositories.value = response.body()?.items as ArrayList<Repository>?

        stopSearchAnimation()
    }


    private fun onSearchEntered(value: String?) {
        if (!value.isNullOrEmpty() && value.length >= MINIMUM_CHARACTERS) {
            currentText = value
            searchGithubRepositories(value, 1)
        } else {
            cancelOnGoingRequest()
            emptyRepositoriesList()
            stopSearchAnimation()
            stopLoadingMoreAnimation()
        }
    }

    private fun emptyRepositoriesList() {
        _repositories.value = ArrayList()
        _repositoriesTotalCount.value = 0
        currentPage = 0
        lastPage = 0
    }


    fun loadMoreRepositories() {
        if (currentPage < lastPage && !isLoadingMore && !isLoading.value!!) {
            sendLoadMoreRequest()
        }
    }

    private fun sendRequest(
        filterText: String,
        page: Int,
        startLoadingAnimation: () -> Unit,
        onSuccess: (Response<RepositorySearchResponse>) -> Unit,
        onException: () -> Unit,
        onComplete: () -> Unit
    ) {
        startLoadingAnimation()
        cancelOnGoingRequest()
        currentRequest = coroutineScope.launch {
            getPropertiesDeferred = githubApi.searchForRepositories(
                filterText,
                PER_PAGE, page
            )

            try {
                val response = getPropertiesDeferred!!.await()

                if (response.isSuccessful) {
                    Log.d("response", "isSuccessful")

                    onSuccess(response)

                }
            } catch (e: CancellationException) {
                Log.d("response", "CancellationException: " + e.message)

            } catch (e: Exception) {
                Log.d("response", "exception: " + e.message)
                onException()
            }
            onComplete()
        }

    }

    private fun cancelOnGoingRequest() {
        if (getPropertiesDeferred != null) {
            if (getPropertiesDeferred!!.isActive) {
                getPropertiesDeferred!!.cancel()

            }
        }
    }

    private fun sendLoadMoreRequest() {
        Timber.i("sendLoadMoreRequest")

        sendRequest(
            currentText,
            currentPage + 1,
            ::startLoadingMoreAnimation,
            ::onSuccessLoadingMore,
            {},
            ::stopLoadingMoreAnimation
        )
    }


    private fun onSuccessLoadingMore(response: Response<RepositorySearchResponse>) {


        val newList = repositories.value
        newList!!.addAll(response.body()?.items!!)
        _repositories.value = newList

        currentPage++
    }

    private fun startLoadingMoreAnimation() {
        isLoadingMore = true

        val newList = repositories.value
        newList!!.add(loadingRepository)
        _repositories.value = newList
    }

    private fun stopLoadingMoreAnimation() {
        isLoadingMore = false
        _repositories.value!!.remove(loadingRepository)
    }

    private fun startSearchAnimation() {
        _isLoading.value = true
    }

    private fun stopSearchAnimation() {
        _isLoading.value = false
    }


    /**
     * Called when the ViewModel is dismantled.
     * At this point, we want to cancel all coroutines;
     * otherwise we end up with processes that have nowhere to return to
     * using memory and resources.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}