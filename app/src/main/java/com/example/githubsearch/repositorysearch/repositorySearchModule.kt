package com.example.githubsearch.repositorysearch

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val repositorySearchModule = module {
    viewModel { GithubRepositorySearchViewModel() }

}

