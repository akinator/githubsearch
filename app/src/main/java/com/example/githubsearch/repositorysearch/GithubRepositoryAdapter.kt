package com.example.githubsearch.repositorysearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.githubsearch.R
import com.example.githubsearch.dataobjects.Repository
import com.example.githubsearch.dataobjects.LOADING_REPOSITORY_ID
import com.example.githubsearch.utils.Util


const val VIEW_TYPE_ITEM = 0
const val VIEW_TYPE_LOADING = 1

class GithubRepositoryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_item, parent, false)

                return ViewHolder(
                    view
                )
            }
        }

        private val fullName: TextView = itemView.findViewById(R.id.textView_full_name)
        val name: TextView = itemView.findViewById(R.id.textView_name)
        private val description: TextView = itemView.findViewById(R.id.textView_description)
        private val language: TextView = itemView.findViewById(R.id.textView_language)


        fun bind(item: Repository) {
            val res = itemView.context.resources

            fullName.text = item.fullName
            name.text = item.name
            description.text =
                if (item.language.isNullOrEmpty()) res.getString(R.string.list_item_no_description) else item.description
            language.text = res.getString(
                R.string.list_item_language,
                if (item.language.isNullOrEmpty()) "/" else item.language
            )

            itemView.setOnClickListener { Util.openNewTabWindow(item.htmlUrl, itemView.context) }
        }
    }

    class LoadingViewHolder private constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        companion object {
            fun from(parent: ViewGroup): LoadingViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.list_item_loading, parent, false)

                return LoadingViewHolder(
                    view
                )
            }

        }

    }


    var data = listOf<Repository>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            val item = data[position]
            holder.bind(item)
        } /*else if (holder is LoadingViewHolder) {
            //showLoadingView
        }*/

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val viewHolder : RecyclerView.ViewHolder

        if (viewType == VIEW_TYPE_ITEM) {
            viewHolder = ViewHolder.from(parent)
        } else {
            viewHolder = LoadingViewHolder.from(parent)
        }
        return viewHolder
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    override fun getItemViewType(position: Int): Int {
        return if (data[position].id == LOADING_REPOSITORY_ID) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

}
