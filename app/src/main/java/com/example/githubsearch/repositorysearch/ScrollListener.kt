package com.example.githubsearch.repositorysearch

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Class to notify GithubRepositorySearchViewModel to the list when reach the last item.
 */
abstract class ScrollListener
/**
 * Supporting only LinearLayoutManager.
 *
 * @param layoutManager
 */(var layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
            reachedEndOfList()
        }
    }

    abstract fun reachedEndOfList()
}