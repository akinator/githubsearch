package com.example.githubsearch.repositorysearch

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubsearch.databinding.FragmentGithubSearchRepositoryBinding
import com.example.githubsearch.utils.Util
import kotlinx.android.synthetic.main.fragment_github_search_repository.*
import org.koin.android.viewmodel.ext.android.viewModel

class GithubRepositorySearchFragment : Fragment() {

    private val githubRepositorySearchViewModel: GithubRepositorySearchViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentGithubSearchRepositoryBinding =
            FragmentGithubSearchRepositoryBinding.inflate(inflater)

        // To use the View Model with data binding, you have to explicitly
        // give the binding object a reference to it.
        binding.viewModel = githubRepositorySearchViewModel

        // Specify the current activity as the lifecycle owner of the binding.
        // This is necessary so that the binding can observe LiveData updates.
        binding.lifecycleOwner = this

        val adapter = GithubRepositoryAdapter()
        binding.recyclerView.adapter = adapter

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                githubRepositorySearchViewModel.searchKeyword = s.toString()
            }

        })

        binding.recyclerView.addOnScrollListener(object :
            ScrollListener(binding.recyclerView.layoutManager as LinearLayoutManager) {
            override fun reachedEndOfList() {
                githubRepositorySearchViewModel.loadMoreRepositories()
            }
        })

        githubRepositorySearchViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (it) {
                    binding.progressBarHorizontal.visibility = View.VISIBLE
                } else
                    binding.progressBarHorizontal.visibility = View.GONE
            }
        })


        githubRepositorySearchViewModel.repositories.observe(viewLifecycleOwner, Observer {
            it?.let {
                recyclerView.post{
                    adapter.data = it
                }

                if (githubRepositorySearchViewModel.isLoading.value!!) {
                    binding.recyclerView.smoothScrollToPosition(0)
                    Util.runLayoutAnimation(binding.recyclerView)
                } /*else {
                }*/
            }
        })

        return binding.root
    }
}