package com.example.githubsearch.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView


object Util {
    fun openNewTabWindow(urls: String?, context: Context) {
        if (urls.isNullOrEmpty()) {
            Toast.makeText(context, "Missing URL", Toast.LENGTH_SHORT).show()
        } else {
            val uris = Uri.parse(urls)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            val b = Bundle()
            b.putBoolean("new_window", true)
            intents.putExtras(b)
            context.startActivity(intents)
        }
    }

    fun runLayoutAnimation(recyclerView: RecyclerView) {
        val context = recyclerView.context
        val controller: LayoutAnimationController =
            AnimationUtils.loadLayoutAnimation(
                context,
                com.example.githubsearch.R.anim.layout_recyclerview_item_animation
            )
        recyclerView.layoutAnimation = controller
        recyclerView.scheduleLayoutAnimation()
    }

}