package com.example.githubsearch.networking

import com.example.githubsearch.dataobjects.RepositorySearchResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/*private const val GITHUB_USER: String = "testuser1githubsearch"
private const val USER_PW: String = "a7Hodzt/2!dgOU5458sw"

private const val BASE_URL = "https://api.github.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val interceptor = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}
val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(
    BasicAuthInterceptor(
        GITHUB_USER,
        USER_PW
    )
).build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .client(client)
    .build()



object GithubApi {
    val retrofitService: GithubApiService by lazy {
        retrofit.create(
            GithubApiService::class.java
        )
    }
}*/

interface GithubApi {

    @GET("search/repositories")
    fun searchForRepositories(
        @Query("q") filterText: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): Deferred<Response<RepositorySearchResponse>>
}



