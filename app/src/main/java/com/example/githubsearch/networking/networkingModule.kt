package com.example.githubsearch.networking

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val GITHUB_USER: String = "testuser1githubsearch"
private const val USER_PW: String = "a7Hodzt/2!dgOU5458sw"

private const val BASE_URL = "https://api.github.com/"

val networkingModule = module {

    factory { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
    factory { provideBasicAuthInterceptor() }

    single { moshi() }
    single { provideOkHttpClient(loggingInterceptor = get(), basicAuthInterceptor = get()) }
    single { provideApi(okHttpClient = get(), moshi = get()) }
}

private fun provideBasicAuthInterceptor() = BasicAuthInterceptor(GITHUB_USER, USER_PW)

private fun provideOkHttpClient(
    loggingInterceptor: HttpLoggingInterceptor,
    basicAuthInterceptor: BasicAuthInterceptor
) = OkHttpClient.Builder()
    .addInterceptor(loggingInterceptor)
    .addInterceptor(basicAuthInterceptor).build()

private fun provideApi(okHttpClient: OkHttpClient, moshi: Moshi) = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .client(okHttpClient)
    .build().create(
        GithubApi::class.java
    )

private fun moshi() = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()