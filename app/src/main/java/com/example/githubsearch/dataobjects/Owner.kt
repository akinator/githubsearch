package com.example.githubsearch.dataobjects


data class Owner(
    val id: Int
)