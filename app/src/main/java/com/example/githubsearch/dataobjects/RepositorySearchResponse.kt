package com.example.githubsearch.dataobjects

import com.squareup.moshi.Json

// Data Model for the Response returned from the Api
data class RepositorySearchResponse(
    @Json(name = "total_count") val totalCount: Int,
    @Json(name = "incomplete_results") val incompleteResults: Boolean,
    val items: List<Repository>
)