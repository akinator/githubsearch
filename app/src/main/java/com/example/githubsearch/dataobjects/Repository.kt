package com.example.githubsearch.dataobjects

import com.squareup.moshi.Json


const val LOADING_REPOSITORY_ID = -1

data class Repository(
    val id: Int,
    val name: String?,
    @Json(name = "full_name") val fullName: String?,
    val language: String?,
    val description: String?,
    @Json(name = "html_url") val htmlUrl: String?,
    @Json(name = "stargazers_count") val starGazersCount: Int,
    val owner: Owner?
) {
    constructor(id: Int) : this(id, null, null, null, null, null, 0, null)

}